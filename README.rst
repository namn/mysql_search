mysql_search
============

This is a quick hack to automatically create fulltext indexes in MySQL with
South.


How to use
==========

Follow these simple steps:

#. Declare your ``TextField`` and ``CharField`` as ``FulltextTextField`` and
   ``FulltextCharField`` respectively::

    from django.db import models
    from mysql_search.models import (FulltextTextField, FulltextCharField,
        SearchManager)

    class Message(models.Model):
        title = FulltextCharField(max_length=160, default='')
        content = FulltextTextField(default='')

#. You can either use an included object manager ``SearchManager`` or use
   Django `search field lookup`_. To use the object manager, add it to your
   model::

    #
        objects = SearchManager(['title'])

   _`search field lookup`: https://docs.djangoproject.com/en/1.0/ref/models/querysets/#search

#. Then query with either::

    Message.objects.filter(title__search='mysql') # BOOLEAN MODE

   Or when your object manager is ``SearchManager``::

    Message.objects.search('mysql') # NATURAL LANGUAGE

#. Set ``SOUTH_DATABASE_ADAPTERS`` for your database to ``mysql_search.south``.
   This is done in your ``settings.py`` file::

    SOUTH_DATABASE_ADAPTERS = {
        'default': 'mysql_search.south'
    }

#. Convert your application to use South. Please read `South's documentation`_
   for how to do this step.

    _`South's documentation`: http://south.readthedocs.org

Caveats
=======

This only works with MySQL.

This only works with MyISAM tables. When migration is run, the tables will be
silently converted to MyISAM. If you need InnoDB, create a different model just
to hold fulltext indexes.


License
=======

Copyright 2013 Nam T. Nguyen

Distributed under the terms of the BSD license.
