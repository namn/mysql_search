'''This module adds default implementation for fulltext fields and an object
manager to search on those fields.

Idea inspired from Mercurytide                                                  
                                                                                
http://www.mercurytide.co.uk/news/article/django-full-text-search/

'''

import logging

from django.db import models, connection


class FulltextTextField(models.TextField):
    '''This is a marker class, it does not add any logic to ``TextField``.

    This class is used to distinguish between indexable and non-indexable text
    fields. If a field needs fulltext search on it, it should be declared as a
    ``SearchableTextField`` so that automatic index creation can be done.'''

    pass


class FulltextCharField(models.CharField):
    '''Ditto'''

    pass


class SearchQuerySet(models.query.QuerySet):
    '''This QuerySet adds ``search`` query over several fields.

    This QuerySet is different from the ``__search`` keyword parameter in that
    it uses MySQL natural search mode, instead of Boolean mode.

    It is best to use this QuerySet with the ``SearchManager``.

    You can also subclass to query for relevancy from the search.
    Method ``select_relevance`` should return a dictionary that will be passed
    to ``extra`` in ``search``. A simple example could be::

        def select_relevance(self, current_args, match_expr, text):
            current_args['select'] = {'relevance': match_expr}
            current_args['select_params'] = [text]
            return current_args

    See also: class:`SearchManager`

    '''


    def set_fields(self, fields):
        self.search_fields = fields

    def search(self, text):
        meta = self.model._meta
        columns = [meta.get_field(name, many_to_many=False).column
            for name in self.search_fields]
        full_names = ['%s.%s' % (connection.ops.quote_name(meta.db_table),
            connection.ops.quote_name(column)) for column in columns]
        full_names = ', '.join(full_names)
        match_expr = 'MATCH(%s) AGAINST (%%s)' % full_names
        kw_args = {'where': [match_expr], 'params': [text]}
        kw_args = self.select_relevance(kw_args, match_expr, text)
        return self.extra(**kw_args)

    def select_relevance(self, current_args, match_expr, text):
        return current_args


class SearchManager(models.Manager):
    '''This Manager is used to create ``SearchQuerySet`` for an already created
    fulltext index on a MySQL table.

    The default QuerySet class is ``SearchQuerySet``. It can be set to any
    subclass of ``SearchQuerySet`` (in case you want to fetch relevance score).

    '''

    def __init__(self, fulltext_fields=[], queryset_class=SearchQuerySet):
        super(SearchManager, self).__init__()
        self.search_fields = fulltext_fields
        self.queryset_class = queryset_class

    def get_query_set(self):
        qs = self.queryset_class(self.model)
        qs.set_fields(self.search_fields)
        return qs

    def search(self, text):
        qs = self.get_query_set()
        return qs.search(text)

    def __getattr__(self, attr):
        if attr.startswith('_'):
            raise NameError('Attribute not found')
        qs = self.get_query_set(self)
        return getattr(qs, attr)
