'''This module adds fulltext indexes to Django models in MySQL with South.

    1. Use South for your app. Set ``SOUTH_DATABASE_ADAPTERS`` for your app
    to ``mysql_search.south``.

    2. Subclass Django fields such as ``TextField``, ``CharField`` and name
    them with prefix ``Fulltext``. You can either create these Field subclasses
    or use the ones included in ``models`` sibling module.

    3. Use ``QuerySet``'s ``__search`` operator or use the included
    ``SearchManager`` in ``models`` module.

'''

from __future__ import absolute_import

import logging
import re

from south import modelsinspector
from south.db import mysql


FULLTEXT_CLASS_NAME_PATTERN = '^.*.?Fulltext[^\.]*$'

def is_fulltext_field(field):
    p = re.compile(FULLTEXT_CLASS_NAME_PATTERN)
    if p.match(field.__class__.__name__):
        return True
    return False


class DatabaseOperations(mysql.DatabaseOperations):

    def create_table(self, table_name, fields):
        super(DatabaseOperations, self).create_table(table_name, fields)
        for field_name, field in fields:
            if is_fulltext_field(field):
                self.create_fulltext_index(table_name, field_name)

    def drop_fulltext_index(self, table_name, column_name):
        # drop fulltext index first before dropping column
        fulltext_index_name = '%s_%s_fulltext' % (table_name, column_name)
        current_indexes = self.execute('SHOW INDEX FROM ' +
            self.quote_name(table_name) +
            ' WHERE key_name = %s', [fulltext_index_name])
        if current_indexes:
            self.execute('DROP INDEX ' + self.quote_name(fulltext_index_name) +
                ' ON ' + self.quote_name(table_name))

    def create_fulltext_index(self, table_name, column_name):
        result = self.execute('SHOW TABLE STATUS LIKE %s', [table_name])
        if result and result[0][1] != 'MyISAM': # engine is not MyISAM
            self.execute('ALTER TABLE ' + self.quote_name(table_name) + 
                ' ENGINE=MyISAM')
        fulltext_index_name = '%s_%s_fulltext' % (table_name, column_name)
        current_indexes = self.execute('SHOW INDEX FROM ' +
            self.quote_name(table_name) +
            ' WHERE key_name = %s', [fulltext_index_name])
        if not current_indexes:
            self.execute('CREATE FULLTEXT INDEX ' +
                self.quote_name(fulltext_index_name) +
                ' ON ' + self.quote_name(table_name) +
                '(' + self.quote_name(column_name) + ')')

    def alter_column(self, table_name, name, field, explicit_name=True,
            ignore_constraints=False):
        super(DatabaseOperations, self).alter_column(table_name, name,
            field, explicit_name)
        if is_fulltext_field(field):
            self.create_fulltext_index(table_name, name)
        else:
            self.drop_fulltext_index(table_name, name)

    def add_column(self, table_name, name, field, keep_default=False):
        super(DatabaseOperations, self).add_column(table_name, name,
            field, keep_default)
        if is_fulltext_field(field):
            self.create_fulltext_index(table_name, name)


modelsinspector.add_introspection_rules([], [FULLTEXT_CLASS_NAME_PATTERN])
