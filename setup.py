from setuptools import setup
name = 'mysql_search'
desc = ('mysql_search is a quick hack to automatically create fulltext indexes'
    ' in MySQL with South.')
setup(
    name=name,
    version='1.0.0',
    author='Nam T. Nguyen',
    author_email='namn@bluemoon.com.vn',
    url='https://bitbucket.org/namn/mysql_search/overview',
    description=desc,
    long_description=desc,
    platforms='Any',
    package_dir={'':'.'},
    packages=['mysql_search'],
    package_data={'': ['README.rst', 'LICENSE']},
    license='BSD',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Database',
        'Framework :: Django',
    ]
)
