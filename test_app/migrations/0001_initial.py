# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Message'
        db.create_table('test_app_message', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sender', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('content', self.gf('mysql_search.models.FulltextTextField')(default='')),
            ('title', self.gf('mysql_search.models.FulltextCharField')(default='', max_length=256)),
        ))
        db.send_create_signal('test_app', ['Message'])


    def backwards(self, orm):
        # Deleting model 'Message'
        db.delete_table('test_app_message')


    models = {
        'test_app.message': {
            'Meta': {'object_name': 'Message'},
            'content': ('mysql_search.models.FulltextTextField', [], {'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'title': ('mysql_search.models.FulltextCharField', [], {'default': "''", 'max_length': '256'})
        }
    }

    complete_apps = ['test_app']