# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Message.content'
        db.alter_column(u'test_app_message', 'content', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Message.title'
        db.alter_column(u'test_app_message', 'title', self.gf('django.db.models.fields.CharField')(max_length=256))

    def backwards(self, orm):

        # Changing field 'Message.content'
        db.alter_column(u'test_app_message', 'content', self.gf('mysql_search.models.FulltextTextField')())

        # Changing field 'Message.title'
        db.alter_column(u'test_app_message', 'title', self.gf('mysql_search.models.FulltextCharField')(max_length=256))

    models = {
        u'test_app.message': {
            'Meta': {'object_name': 'Message'},
            'content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'})
        }
    }

    complete_apps = ['test_app']