# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Message.content'
        db.delete_column(u'test_app_message', 'content')

        # Deleting field 'Message.title'
        db.delete_column(u'test_app_message', 'title')


    def backwards(self, orm):
        # Adding field 'Message.content'
        db.add_column(u'test_app_message', 'content',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Message.title'
        db.add_column(u'test_app_message', 'title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=256),
                      keep_default=False)


    models = {
        u'test_app.message': {
            'Meta': {'object_name': 'Message'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['test_app']