from django.db import models

from mysql_search.models import (FulltextTextField, FulltextCharField,
    SearchManager)


class Message(models.Model):
    sender = models.CharField(max_length=32)
    content = FulltextTextField(default='')
    title = FulltextCharField(max_length=256, default='')

    objects = models.Manager()
    manager = SearchManager(['title'])

    def __unicode__(self):
        return 'Message(%s, %s, %s)' % (self.sender, self.content, self.title)
