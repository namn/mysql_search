# Django settings for test_app
import logging
logging.basicConfig(level=logging.DEBUG)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = 'ignored'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

INSTALLED_APPS = (
    'south',
    'test_app',
)

SOUTH_DATABASE_ADAPTERS = {
    'default': 'mysql_search.south'
}
