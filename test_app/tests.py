'''MySQL search tests.
'''

from django.conf import settings
from django.test import TestCase
from django.db.models import Q

from test_app import models


class Test(TestCase):

    def setUp(self):
        settings.DEBUG = True
        models.Message.objects.get_or_create(id=1, sender='s1',
            content='better stuff', title='hello world')
        models.Message.objects.get_or_create(id=2, sender='s1',
            content='wordly docs', title='mocking bird')
        models.Message.objects.get_or_create(id=3, sender='s1',
            content='awesome book', title='brief history of time')

    def test_extra(self):
        r = list(models.Message.objects.extra(where=['''
            MATCH(title) AGAINST(%s)'''], params=['world']))
        self.assertEqual(1, len(r))
        self.assertEqual(1, r[0].id)

    def test_manager(self):
        r = models.Message.manager.search('world')
        self.assertEqual(1, len(r))
        self.assertEqual(1, r[0].id)

    def test_search(self):
        r = models.Message.objects.filter(title__search='world')
        self.assertEqual(1, len(r))
        self.assertEqual(1, r[0].id)

    def test_other(self):
        r = models.Message.manager.get(id=1)
        self.assertEqual(1, r.id)

    def test_name_error(self):
        try:
            models.Message.manager._underscore
            self.fail()
        except NameError:
            pass

    def test_q(self):
        q = Q(title__search='world') | Q(content__search='wordly')
        r = models.Message.objects.filter(q)
        self.assertEqual(2, len(r))

        meta = models.Message._meta
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute('CREATE FULLTEXT INDEX `_` ON ' + meta.db_table + 
            ' (title, content)')

        r = models.Message.objects.extra(where=['MATCH(title, content) AGAINST (%s)'], params=['world wordly'])
        self.assertEqual(2, len(r))
        cursor.execute('DROP INDEX `_` ON ' + meta.db_table)
